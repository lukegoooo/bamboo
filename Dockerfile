FROM golang:stretch as build-env

LABEL maintainer="lukegoooo@gmail.com"

WORKDIR /go/src/git.autops.xyz/autops/bamboo

COPY go.mod .
COPY go.sum .

RUN GO111MODULE=on go mod download

COPY . .

RUN https_proxy=http://127.0.0.1:1087 GO111MODULE=on go build -v -o bamboo git.autops.xyz/autops/bamboo

FROM d.autops.xyz/base:3.8

COPY --from=build-env /go/src/git.autops.xyz/autops/bamboo/bamboo /app/bamboo

# Run the app by default when the container starts
CMD /app/bamboo