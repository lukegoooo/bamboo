package misc

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/golang/glog"
	uuid "github.com/satori/go.uuid"

	"baliance.com/gooxml/color"
	"baliance.com/gooxml/common"
	"baliance.com/gooxml/document"
	"baliance.com/gooxml/measurement"
	"baliance.com/gooxml/schema/soo/wml"

	"github.com/PuerkitoBio/goquery"
	"github.com/spf13/cast"
)

var transferClient = func() *http.Client {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	return &http.Client{Transport: tr, Timeout: 30 * time.Second}
}()

//CreateDocxByHTML ...
func CreateDocxByHTML(html []byte, pdfAddress string) error {
	doc, err := goquery.NewDocumentFromReader(bytes.NewReader(html))
	if err != nil {
		return err
	}

	docu, para, run := createDocx()

	doc.Find("body").Find("p,h1,h2,ul,u,strong").Each(func(index int, ele *goquery.Selection) {
		var err error
		html, err := ele.Html()
		if err != nil {
			glog.Error(err)
			return
		}

		if ele.Parent().Get(0).Data != "body" {
			return
		}

		para = docu.AddParagraph()
		run = para.AddRun()

		class, exist := ele.Attr("class")
		if exist {
			for _, once := range strings.Split(class, " ") {
				if strings.HasPrefix(once, "ql-heading-") {
					para.Properties().SetHeadingLevel(getTitleLevel(class))
				}
				if strings.HasPrefix(once, "ql-align-") {
					para.Properties().SetAlignment(wml.ST_JcCenter)
				}
			}

			if html != "" {
				if n := prefixIndexHTMLElement(html); n <= len(html) {
					run.AddText(html[:n])
					html = html[n:]
				}
			}
		} else {
			if prefixNOContainHTMLElement(html) == "" {
				run.AddText(html[:prefixIndexHTMLElement(html)])
				html = html[prefixIndexHTMLElement(html):]
			}
		}

		if html == "<br>" {
			para = docu.AddParagraph()
			run = para.AddRun()
		}

		reg := regexp.MustCompile(`<[^>]+>`)
		labels := reg.FindAllString(html, -1)

		var (
			nodeHTML *goquery.Document
			endIndex = -1
		)

		for index, label := range labels {
			if index <= endIndex {
				continue
			}

			if strings.HasPrefix(label, "<span") {
				if strings.HasPrefix(label, "<span") {
					endIndex = endIndexForLabel(labels, index, "</span>")
					run = para.AddRun()
				}

				nodeHTML, err = getdocumentByElement(html, "<span")
				if err != nil {
					glog.Error(err)
					return
				}

				html = getHTMLByIndex(html, "/span>")

				nodeHTML.Find("span").Each(func(i int, childNodeHTML *goquery.Selection) {
					run = para.AddRun()
					childNodeHTML.FilterFunction(func(i int, s *goquery.Selection) bool {
						for _, attr := range s.Get(0).Attr {
							//style
							if attr.Key == "style" {
								for _, style := range strings.Split(attr.Val, ";") {
									if strings.Contains(style, "background-color") {
										reg := regexp.MustCompile(`\d+\.?\d*`)
										r := reg.FindAllString(style, -1)
										if len(r) == 3 {
											run.Properties().SetHighlight(wml.ST_HighlightColorRed)
										}
									}

									//color
									if strings.Contains(style, "color:") {
										reg := regexp.MustCompile(`\d+\.?\d*`)
										r := reg.FindAllString(style, -1)
										if len(r) == 3 {
											run.Properties().SetColor(color.RGB(cast.ToUint8(r[0]), cast.ToUint8(r[1]), cast.ToUint8(r[2])))
										}
									}

									//font-size
									if strings.Contains(style, "font-size") {
										reg := regexp.MustCompile(`\d+\.?\d*`)
										r := reg.FindAllString(style, -1)
										if len(r) == 1 {
											r0, _ := strconv.ParseFloat(r[0], 64)
											run.Properties().SetSize(measurement.Distance(r0))
										}
									}

								}

								//class
								if attr.Key == "class" {
									for _, style := range strings.Split(attr.Val, ";") {
										if strings.Contains(style, "ql-font-") {
											run.Properties().SetFontFamily(strings.TrimPrefix(class, "ql-font-"))
										}
									}
								}
							}
						}
						return len(s.Get(0).Attr) > 0
					})
					if val, ok := childNodeHTML.Find("img").Attr("src"); ok {
						run = para.AddRun()
						path := setImageAddr(val)
						if path == "" {
							return
						}
						img, err := common.ImageFromFile(path)
						if err != nil {
							return
						}

						iref, err := docu.AddImage(img)
						if err != nil {
							return
						}

						anchored, err := run.AddDrawingInline(iref)
						if err != nil {
							return
						}
						anchored.SetSize(5*measurement.Inch, 5*measurement.Inch)
					} else {
						run.AddText(childNodeHTML.Text())
					}
				})

				if prefixNOContainHTMLElement(html) == "" {
					run = para.AddRun()
					run.AddText(html[:prefixIndexHTMLElement(html)])
					html = html[prefixIndexHTMLElement(html):]
				}
			}

			if strings.HasPrefix(label, "<strong") {
				if strings.HasPrefix(label, "<strong") {
					endIndex = endIndexForLabel(labels, index, "</strong>")
					run = para.AddRun()
				}

				nodeHTML, err = getdocumentByElement(html, "<strong")
				if err != nil {
					glog.Error(err)
					return
				}

				html = getHTMLByIndex(html, "/strong>")

				nodeHTML.Find("strong").Each(func(i int, childNodeHTML *goquery.Selection) {
					run = para.AddRun()
					childNodeHTML.FilterFunction(func(i int, s *goquery.Selection) bool {
						for _, attr := range s.Get(0).Attr {
							//style
							if attr.Key == "style" {
								for _, style := range strings.Split(attr.Val, ";") {
									if strings.Contains(style, "background-color") {
										reg := regexp.MustCompile(`\d+\.?\d*`)
										r := reg.FindAllString(style, -1)
										if len(r) == 3 {
											run.Properties().SetHighlight(wml.ST_HighlightColorRed)
										}
									}

									//color
									if strings.Contains(style, "color:") {
										reg := regexp.MustCompile(`\d+\.?\d*`)
										r := reg.FindAllString(style, -1)
										if len(r) == 3 {
											run.Properties().SetColor(color.RGB(cast.ToUint8(r[0]), cast.ToUint8(r[1]), cast.ToUint8(r[2])))
										}
									}

									//font-size
									if strings.Contains(style, "font-size") {
										reg := regexp.MustCompile(`\d+\.?\d*`)
										r := reg.FindAllString(style, -1)
										if len(r) == 1 {
											r0, _ := strconv.ParseFloat(r[0], 64)
											run.Properties().SetSize(measurement.Distance(r0))
										}
									}

								}

								//class
								if attr.Key == "class" {
									for _, style := range strings.Split(attr.Val, ";") {
										if strings.Contains(style, "ql-font-") {
											run.Properties().SetFontFamily(strings.TrimPrefix(class, "ql-font-"))
										}
									}
								}
							}
						}
						return len(s.Get(0).Attr) > 0
					})

					run.AddText(childNodeHTML.Text())
				})
			}

			if strings.HasPrefix(label, "<u") {
				if strings.HasPrefix(label, "<u") {
					endIndex = endIndexForLabel(labels, index, "</u>")
					run = para.AddRun()
				}

				nodeHTML, err = getdocumentByElement(html, "<u")
				if err != nil {
					glog.Error(err)
					return
				}

				html = getHTMLByIndex(html, "/u>")

				run = para.AddRun()
				nodeHTML.Find("u").Each(func(i int, childNodeHTML *goquery.Selection) {
					childNodeHTML.FilterFunction(func(i int, s *goquery.Selection) bool {
						for _, attr := range s.Get(0).Attr {
							if attr.Key == "style" {
								for _, style := range strings.Split(attr.Val, ";") {
									//font-size
									reg := regexp.MustCompile(`\d+\.?\d*`)
									r := reg.FindAllString(style, -1)
									if len(r) == 1 {
										r0, _ := strconv.ParseFloat(r[0], 64)
										run.Properties().SetSize(measurement.Distance(r0))
									}
								}
							}
						}

						if strings.HasPrefix(label, "<em") {
							run.Properties().SetEmboss(true)
						} else if strings.HasPrefix(label, "<u") {
							run.Properties().SetUnderline(wml.ST_UnderlineWords, color.Black)
						}

						return len(s.Get(0).Attr) > 0
					})
				})
				run.AddText(nodeHTML.Text())
			}

			if strings.HasPrefix(label, "<em") {
				if strings.HasPrefix(label, "<em") {
					endIndex = endIndexForLabel(labels, index, "</em>")
					run = para.AddRun()
				}

				nodeHTML, err = getdocumentByElement(html, "<em")
				if err != nil {
					glog.Error(err)
					return
				}

				html = getHTMLByIndex(html, "/em>")

				run = para.AddRun()
				run.Properties().SetItalic(true)
				nodeHTML.Find("em").Each(func(i int, childNodeHTML *goquery.Selection) {
					childNodeHTML.FilterFunction(func(i int, s *goquery.Selection) bool {
						for _, attr := range s.Get(0).Attr {
							if attr.Key == "style" {
								for _, style := range strings.Split(attr.Val, ";") {
									//font-size
									reg := regexp.MustCompile(`\d+\.?\d*`)
									r := reg.FindAllString(style, -1)
									if len(r) == 1 {
										r0, _ := strconv.ParseFloat(r[0], 64)
										run.Properties().SetSize(measurement.Distance(r0))
									}
								}
							}
						}
						return len(s.Get(0).Attr) > 0
					})

					ret, _ := childNodeHTML.Html()
					if strings.HasPrefix(ret, "<u") {
						run.Properties().SetUnderline(wml.ST_UnderlineWords, color.Black)
					}
				})

				run.AddText(nodeHTML.Text())
			}

			if strings.HasPrefix(label, "<li") {
				if strings.HasPrefix(label, "<li") {
					endIndex = endIndexForLabel(labels, index, "</li>")
					run = para.AddRun()
				}

				nodeHTML, err = getdocumentByElement(html, "<li")
				if err != nil {
					glog.Error(err)
					return
				}

				html = getHTMLByIndex(html, "/li>")
				run = para.AddRun()
				nodeHTML.Find("li").Each(func(i int, childNodeHTML *goquery.Selection) {
					childNodeHTML.FilterFunction(func(i int, s *goquery.Selection) bool {
						for _, attr := range s.Get(0).Attr {
							if attr.Key == "style" {
								for _, style := range strings.Split(attr.Val, ";") {
									//font-size
									reg := regexp.MustCompile(`\d+\.?\d*`)
									r := reg.FindAllString(style, -1)
									if len(r) == 1 {
										r0, _ := strconv.ParseFloat(r[0], 64)
										run.Properties().SetSize(measurement.Distance(r0))
									}
								}
							}
						}
						return len(s.Get(0).Attr) > 0
					})

					_, ok := childNodeHTML.Attr("class")
					if ok {
						para.SetNumberingLevel(0)
						para.SetNumberingDefinition(docu.Numbering.Definitions()[0])
					}
				})

				run.AddText(nodeHTML.Text())
			}

			if strings.HasPrefix(label, "<img") {
				if strings.HasPrefix(label, "<img") {
					endIndex = endIndexForLabel(labels, index, "</img>")
					run = para.AddRun()
				}

				nodeHTML, err = getdocumentByElement(html, "<img")
				if err != nil {
					glog.Error(err)
					return
				}

				html = getHTMLByIndex(html, ">")

				run = para.AddRun()
				if val, ok := nodeHTML.Find("img").Attr("src"); ok {
					path := setImageAddr(val)
					if path == "" {
						return
					}
					img, err := common.ImageFromFile(path)
					if err != nil {
						return
					}

					iref, err := docu.AddImage(img)
					if err != nil {
						return
					}
					anchored, err := run.AddDrawingInline(iref)
					if err != nil {
						return
					}
					anchored.SetSize(5*measurement.Inch, 5*measurement.Inch)
				}
			}

			if strings.HasPrefix(label, "<a") {
				if strings.HasPrefix(label, "<a") {
					endIndex = endIndexForLabel(labels, index, "</a>")
					run = para.AddRun()
				}

				nodeHTML, err = getdocumentByElement(html, "<a")
				if err != nil {
					glog.Error(err)
					return
				}

				html = getHTMLByIndex(html, "/a>")

				run = para.AddRun()
				nodeHTML.Find("a").Each(func(i int, childNodeHTML *goquery.Selection) {
					hl := para.AddHyperLink()
					run = hl.AddRun()
					childNodeHTML.FilterFunction(func(i int, s *goquery.Selection) bool {
						for _, attr := range s.Get(0).Attr {
							if attr.Key == "style" {
								for _, style := range strings.Split(attr.Val, ";") {
									//font-size
									reg := regexp.MustCompile(`\d+\.?\d*`)
									r := reg.FindAllString(style, -1)
									if len(r) == 1 {
										r0, _ := strconv.ParseFloat(r[0], 64)
										run.Properties().SetSize(measurement.Distance(r0))
									}

									//color
									if strings.Contains(style, "color:") {
										reg := regexp.MustCompile(`\d+\.?\d*`)
										r := reg.FindAllString(style, -1)
										if len(r) == 3 {
											run.Properties().SetColor(color.RGB(cast.ToUint8(r[0]), cast.ToUint8(r[1]), cast.ToUint8(r[2])))
										}
									}
								}
							}

							if attr.Key == "href" {
								hl.SetTarget(attr.Val)
								run.Properties().SetUnderline(wml.ST_UnderlineWords, color.Black)
								run.Properties().SetStyle("Hyperlink")
							}

							//class
							if attr.Key == "class" {
								for _, style := range strings.Split(attr.Val, ";") {
									if strings.Contains(style, "ql-font-") {
										run.Properties().SetFontFamily(strings.TrimPrefix(class, "ql-font-"))
									}
								}
							}
						}
						return len(s.Get(0).Attr) > 0
					})
				})

				run.AddText(nodeHTML.Text())
			}

			if prefixNOContainHTMLElement(html) == "" {
				run = para.AddRun()
				run.AddText(html)
			}

			if label == "<br/>" {
				para = docu.AddParagraph()
				run = para.AddRun()
			}
		}
	})

	docu.SaveToFile(pdfAddress)
	return nil
}

func getHTMLByIndex(html, label string) string {
	index := strings.Index(html, label) + len(label)
	if index > len(html) {
		return ""
	}

	return html[index:]
}

func getLabelIndex(html, substr string) int {
	var pos, index int
	for i := 0; i < 2; i++ {
		pos = strings.Index(html, substr)
		html = html[pos+1:]
		index = pos + index + 1
	}

	return index - 1
}

func cleanFootLabels(labels []string) []string {
	frontLables := make([]string, 0, len(labels))
	for _, label := range labels {
		if label != "</span>" && label != "</u>" && label != "</strong>" && label != "</em>" {
			frontLables = append(frontLables, label)
		}
	}

	return frontLables
}

func prefixNOContainHTMLElement(content string) string {
	content = strings.TrimSpace(content)
	for _, once := range []string{"<br", "<span", "<a", "<u", "<ul", "<img", "<em", "<strong", "<br", "<li"} {
		if strings.HasPrefix(content, once) {
			return once
		}
	}
	return ""
}

func prefixIndexHTMLElement(content string) int {
	for _, once := range []string{"<br", "<span", "<a", "<u", "<ul", "<img", "<em", "<strong", "<li"} {
		if n := strings.Index(content, once); n > -1 {
			return n
		}
	}

	return len(content)
}

func suffixNOContainHTMLElement(content string) string {
	for _, once := range []string{"br>", "/span>", "/a>", "/u>", "/ul>", "/img>", "/em>", "<li"} {
		if strings.HasSuffix(content, once) {
			return once
		}
	}
	return ""
}

func getdocumentByElement(html string, label string) (*goquery.Document, error) {
	substr, l := getEndLabel(label)
	htmlIndex, htmlLastIndex := strings.Index(html, label), strings.Index(html, substr)+l
	elementHTML := html[htmlIndex:htmlLastIndex]

	return goquery.NewDocumentFromReader(strings.NewReader(elementHTML))
}

func getEndLabel(startLabel string) (string, int) {
	if strings.HasPrefix(startLabel, "<span") {
		return "</span>", len("</span>")
	}
	if strings.HasPrefix(startLabel, "<strong") {
		return "</strong>", len("</strong>")
	}
	if strings.HasPrefix(startLabel, "<u") {
		return "</u>", len("</u>")
	}
	if strings.HasPrefix(startLabel, "<em") {
		return "</em>", len("</em>")
	}
	if strings.HasPrefix(startLabel, "<li") {
		return "</li>", len("</li>")
	}
	if strings.HasPrefix(startLabel, "<a") {
		return "</a>", len("</a>")
	}
	if strings.HasPrefix(startLabel, "<img") {
		return ">", len(">")
	}

	return "", 0
}

func abbLabel(label string) string {
	if strings.HasPrefix(label, "<span") {
		return "span"
	}
	if strings.HasPrefix(label, "<strong") {
		return "strong"
	}
	if strings.HasPrefix(label, "<u") {
		return "u"
	}
	if strings.HasPrefix(label, "<em") {
		return "em"
	}

	return ""
}

func getTitleLevel(class string) int {
	switch strings.TrimPrefix(class, "ql-heading-") {
	case "h1":
		return 1
	case "h2":
		return 2
	case "h3":
		return 3
	case "h4":
		return 4
	case "h5":
		return 5
	case "h6":
		return 6
	default:
		return 5
	}
}

func endIndexForLabel(labels []string, startIndex int, label string) int {
	for index, once := range labels {
		if index > startIndex && once == label {
			return index
		}
	}
	return -1
}

func existFind(childHTML, parentElement string) bool {
	if strings.HasPrefix(childHTML, "<ul") && parentElement == "p" {
		return true
	}

	if strings.HasPrefix(childHTML, "<u") && (parentElement == "p" || parentElement == "ul") {
		return true
	}

	if strings.HasPrefix(childHTML, "<strong") && (parentElement == "p" || parentElement == "u") {
		return true
	}

	return false
}

func createDocx() (*document.Document, document.Paragraph, document.Run) {
	docu := document.New()
	para := docu.AddParagraph()
	run := para.AddRun()

	return docu, para, run
}

func setImageAddr(url string) string {
	resp, err := transferClient.Get(url)
	if err != nil {
		glog.Error(err)
		return ""
	}

	defer resp.Body.Close()
	bs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		glog.Error(err)
		return ""
	}

	ext := ".jpg"
	if resp.Header.Get("Content-Type") == "image/gif" {
		ext = ".gif"
	}

	fileName := fmt.Sprintf("/tmp/%s%s", uuid.NewV4().String(), ext)
	file, err := os.OpenFile(fileName, os.O_CREATE|os.O_RDWR, os.ModePerm)
	if err != nil {
		glog.Error(err)
		return ""
	}

	defer file.Close()

	if _, err = file.Write(bs); err != nil {
		return ""
	}

	return fileName
}
