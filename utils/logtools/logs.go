package logtools

import (
	"flag"
	"log"

	"github.com/golang/glog"
)

// GoLogLevel is the default v-level to write go's log out
var GoLogLevel = glog.Level(4)

func init() {
	flag.Set("logtostderr", "true")
}

// GlogWriter serves as a bridge between the standard log package and the glog package.
type GlogWriter glog.Level

// Write implements the io.Writer interface.
func (writer GlogWriter) Write(data []byte) (n int, err error) {
	glog.V(glog.Level(writer)).Info(string(data))
	return len(data), nil
}

// InitLogs initializes logs the way we want for restis.
func InitLogs() {
	log.SetOutput(GlogWriter(GoLogLevel))
	log.SetFlags(0)
}

// FlushLogs flushes logs immediately.
func FlushLogs() {
	glog.Flush()
}

// NewLogger creates a new log.Logger which sends logs to glog.Info.
func NewLogger(v int32, prefix string) *log.Logger {
	return log.New(GlogWriter(glog.Level(v)), prefix, 0)
}
