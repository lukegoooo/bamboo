package api

import (
	"crypto/tls"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"strings"

	"git.autops.xyz/autops/bamboo/utils/misc"

	"github.com/gin-gonic/gin"
	"github.com/satori/go.uuid"
)

var (
	imgRE         = regexp.MustCompile(`<img[^>]+\bsrc=["']([^"']+)["']`)
	videoRE       = regexp.MustCompile(`<video[^>]+\bsrc=["']([^"']+)["']`)
	toMapleClient = func() *http.Client {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		return &http.Client{Transport: tr}
	}()
)

type exportDownLoadReq struct {
	Name    string `json:"name"`
	Content string `json:"content"`
}

//NewHandler ...
func NewHandler(url string) *Handler {
	return &Handler{URL: url}
}

//Handler ...
type Handler struct {
	URL string
}

//HandleDownloadPDF ...
func (h *Handler) HandleDownloadPDF(ctx *gin.Context) {
	var r exportDownLoadReq
	if err := ctx.BindJSON(&r); err != nil {
		ctx.JSON(http.StatusOK, result(err))
		return
	}

	tmpHTMLFileName, tmpPDFFileName := fmt.Sprintf("/tmp/%s.html", uuid.NewV4().String()), fmt.Sprintf("/tmp/%s.pdf", uuid.NewV4().String())
	file, err := os.OpenFile(tmpHTMLFileName, os.O_CREATE|os.O_RDWR, os.ModePerm)
	if err != nil {
		ctx.JSON(http.StatusOK, result(err))
		return
	}

	content, err := base64.StdEncoding.DecodeString(r.Content)
	if err != nil {
		ctx.JSON(http.StatusOK, result(err))
		return
	}

	images, videos := findImagesOrVideos(string(content), "img"), findImagesOrVideos(string(content), "video")
	processContent := h.processURLOnMaple(string(content), append(images, videos...))

	file.WriteString(processContent)

	bs, err := misc.CreatePDFByHTML(h.URL, tmpHTMLFileName)
	if err != nil {
		ctx.JSON(http.StatusOK, result(err))
		return
	}

	pdfFile, err := os.OpenFile(tmpPDFFileName, os.O_CREATE|os.O_RDWR, os.ModePerm)
	if err != nil {
		ctx.JSON(http.StatusOK, result(err))
		return
	}

	if _, err = pdfFile.Write(bs); err != nil {
		ctx.JSON(http.StatusOK, result(err))
		return
	}

	defer func() {
		file.Close()
		pdfFile.Close()
		os.Remove(tmpHTMLFileName)
		os.Remove(tmpPDFFileName)
	}()

	ctx.Writer.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%s.pdf", r.Name))
	http.ServeFile(ctx.Writer, ctx.Request, tmpPDFFileName)
}

//HandleDownloadDocx ...
func (h *Handler) HandleDownloadDocx(ctx *gin.Context) {
	var r exportDownLoadReq
	if err := ctx.BindJSON(&r); err != nil {
		ctx.JSON(http.StatusOK, result(err))
		return
	}

	tmpDOCXFileName := fmt.Sprintf("/tmp/%s.docx", uuid.NewV4().String())
	bs, err := base64.StdEncoding.DecodeString(r.Content)
	if err != nil {
		ctx.JSON(http.StatusOK, result(err))
		return
	}

	images, videos := findImagesOrVideos(string(bs), "img"), findImagesOrVideos(string(bs), "video")
	processContent := h.processURLOnMaple(string(bs), append(images, videos...))

	if err := misc.CreateDocxByHTML([]byte(processContent), tmpDOCXFileName); err != nil {
		ctx.JSON(http.StatusOK, result(err))
		return
	}

	ctx.Writer.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%s.docx", r.Name))
	http.ServeFile(ctx.Writer, ctx.Request, tmpDOCXFileName)
}

func findImagesOrVideos(htm, label string) []string {
	var imgs [][]string
	if label == "img" {
		imgs = imgRE.FindAllStringSubmatch(htm, -1)
	} else {
		imgs = videoRE.FindAllStringSubmatch(htm, -1)
	}

	out := make([]string, len(imgs))
	for i := range out {
		out[i] = imgs[i][1]
	}
	return out
}

func (h *Handler) processURLOnMaple(content string, urls []string) string {
	newContent := content
	for _, once := range urls {
		u, err := url.Parse(once)
		if err != nil {
			continue
		}

		paths := strings.Split(u.Path, "/")
		if len(paths) > 0 {
			publicURL, err := h.getPublicMediaAddress(paths[len(paths)-1])
			if err != nil {
				continue
			}

			newContent = strings.Replace(newContent, once, publicURL, -1)
		}
	}

	fmt.Println(newContent)

	return newContent
}

func (h *Handler) getPublicMediaAddress(key string) (string, error) {
	resp, err := toMapleClient.Post(fmt.Sprintf(`%s/inner/open/media/%s?token=Ci1q3KYwSuOOmt90WqA=`, "http://127.0.0.1:13348", key), "application/json", nil)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()
	bs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	newKey, _ := strconv.Unquote(string(bs))

	return fmt.Sprintf("%s/media/%s", "http://127.0.0.1:13348", newKey), nil
}
