package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

const (
	//Code reply code
	Code = "code"
	//Message reply message
	Message = "message"
)

var (
	succ = gin.H{Code: ok, Message: "succ"}
)

func result(result interface{}) gin.H {
	switch result.(type) {
	case error:
		return gin.H{
			Code:    http.StatusInternalServerError,
			Message: result.(error).Error(),
		}
	default:
		return gin.H{
			Code:    ok,
			Message: result,
		}
	}
}

const (
	ok = iota
)
