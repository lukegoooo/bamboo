SHELL := /bin/bash
VERSION=git-$(shell git describe --always --dirty)
IMAGE_TAG=$(VERSION)
IAMGE_REPO=d.autops.xyz

all:

dist:
	env GOOS=linux GOARCH=amd64 go build -v -o ./bamboo

distosx:
	env GOOS=darwin GOARCH=amd64 go build -v -o ./bamboo

generate:
	go generate ./core/store/...

ship:
	docker build -t ${IAMGE_REPO}/bamboo:${IMAGE_TAG} .
	docker push ${IAMGE_REPO}/bamboo:${IMAGE_TAG}