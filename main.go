package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"runtime"
	"time"

	"github.com/golang/glog"

	"git.autops.xyz/autops/bamboo/api"
	"git.autops.xyz/autops/bamboo/utils/flagtools"
	"git.autops.xyz/autops/bamboo/utils/logtools"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/spf13/pflag"
	ginglog "github.com/szuecs/gin-glog"
)

var (
	port         uint
	url          string
	allowOrigins []string
)

func init() {
	pflag.UintVar(&port, "port", 13351, "server port")
	pflag.StringVar(&url, "url", "http://10.42.0.13:3000/unoconv/pdf", "unoconv url")
	pflag.StringSliceVar(&allowOrigins, "origins", []string{"http://localhost:9528", "http://localhost:9529", "http://poplar.node3.autops.xyz", "http://poplar.node4.autops.xyz", "https://poplar.node4.autops.xyz", "https://poplar1.node2.autops.xyz", "https://poplar2.node2.autops.xyz", "https://demo.situoyun.com", "http://demo.situoyun.com"}, "allow origins")
}

func main() {
	// init flags and logs
	runtime.GOMAXPROCS(runtime.NumCPU())
	rand.Seed(time.Now().UTC().UnixNano())

	flagtools.InitFlags()
	logtools.InitLogs()
	defer logtools.FlushLogs()

	gin.SetMode(gin.ReleaseMode)
	router := gin.Default()
	router.Use(gin.Recovery())
	router.Use(ginglog.Logger(3 * time.Second))

	router.Use(cors.New(cors.Config{
		AllowMethods:     []string{"GET", "POST", "PUT", "HEAD", "PATCH", "OPTION", "DELETE"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type", "X-TOKEN", "Tus-Extension", "Tus-Resumable", "Tus-Version", "Upload-Length", "Upload-Metadata", "Upload-Offset", "Access-Control-Allow-Origin"},
		AllowCredentials: true,
		// AllowAllOrigins:  true,
		AllowOrigins: allowOrigins,
		MaxAge:       12 * time.Hour,
	}))

	h := api.NewHandler(url)
	v1 := router.Group("/poplar/v2")
	{
		v1.POST("/export/pdf", h.HandleDownloadPDF)
		v1.POST("/export/docx", h.HandleDownloadDocx)
	}

	server := &http.Server{
		Addr:    fmt.Sprintf(":%d", port),
		Handler: router,
	}

	glog.Infof("start server port:%v", port)
	if err := server.ListenAndServe(); err != nil {
		glog.Fatalf("server port:%d error:%v", port, err)
	}
}
